package mohammadrostami.com.app.mykuya.api.service

import android.content.Context
import android.util.Log
import mohammadrostami.com.app.mykuya.R
import mohammadrostami.com.app.mykuya.model.FeaturedProductsModel
import mohammadrostami.com.app.mykuya.model.NewsModel
import mohammadrostami.com.app.mykuya.model.ProductsModel
import mohammadrostami.com.app.mykuya.viewmodel.WebServiceViewModel
import mohammadrostami.com.app.welearnov2.apis.APIClient
import mohammadrostami.com.app.welearnov2.apis.APIInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductsService {
    private lateinit var webServiceViewModel: WebServiceViewModel
    private var apiInterface: APIInterface? = null

    fun main(context: Context, WebServiceViewModel: WebServiceViewModel) {
        this.webServiceViewModel = WebServiceViewModel


        apiInterface = APIClient.getClient()
            ?.create(APIInterface::class.java)


        val call = apiInterface?.getProducts()
        call?.enqueue(object : Callback<ProductsModel> {
            override fun onResponse(call: Call<ProductsModel>, response: Response<ProductsModel>) {
                Log.d("ProductResponse", response.code().toString() + "")

                val productsModel = ProductsModel()
                productsModel.products?.add(context.resources.getDrawable(R.drawable.product_one))
                productsModel.products?.add(context.resources.getDrawable(R.drawable.product_two))
                productsModel.products?.add(context.resources.getDrawable(R.drawable.product_three))
                productsModel.products?.add(context.resources.getDrawable(R.drawable.product_four))
                productsModel.products?.add(context.resources.getDrawable(R.drawable.product_six))
                productsModel.products?.add(context.resources.getDrawable(R.drawable.product_seven))
                productsModel.products?.add(context.resources.getDrawable(R.drawable.product_eight))
                productsModel.products?.add(context.resources.getDrawable(R.drawable.product_nine))
                productsModel.products?.add(context.resources.getDrawable(R.drawable.product_ten))
                productsModel.products?.add(context.resources.getDrawable(R.drawable.product_eleven))
                productsModel.products?.add(context.resources.getDrawable(R.drawable.product_twelve))
                productsModel.products?.add(context.resources.getDrawable(R.drawable.product_thirteen))
                productsModel.products?.add(context.resources.getDrawable(R.drawable.product_fourteen))
                productsModel.products?.add(context.resources.getDrawable(R.drawable.product_fifteen))
                productsModel.products?.add(context.resources.getDrawable(R.drawable.product_sixteen))
                productsModel.products?.add(context.resources.getDrawable(R.drawable.product_seventeen))
                productsModel.products?.add(context.resources.getDrawable(R.drawable.product_ninteen))


                webServiceViewModel.sendProductsResponse(productsModel)
            }

            override fun onFailure(call: Call<ProductsModel>, t: Throwable) {
                call.cancel()
            }
        })

    }


}