package mohammadrostami.com.app.mykuya.interfaces

import android.view.View
import android.widget.ImageView


interface OnItemListener {
    fun onItemSelect(position: Int)
}
