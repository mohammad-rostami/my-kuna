package mohammadrostami.com.app.mykuya.api.service

import android.content.Context
import android.util.Log
import mohammadrostami.com.app.mykuya.R
import mohammadrostami.com.app.mykuya.model.FeaturedProductsModel
import mohammadrostami.com.app.mykuya.viewmodel.WebServiceViewModel
import mohammadrostami.com.app.welearnov2.apis.APIClient
import mohammadrostami.com.app.welearnov2.apis.APIInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.coroutineContext

class FeaturedProductsService {
    private lateinit var webServiceViewModel: WebServiceViewModel
    private var apiInterface: APIInterface? = null


    fun main(context: Context, WebServiceViewModel: WebServiceViewModel) {
        this.webServiceViewModel = WebServiceViewModel


        apiInterface = APIClient.getClient()
            ?.create(APIInterface::class.java)


        val call = apiInterface?.getFeaturedProducts()
        call?.enqueue(object : Callback<FeaturedProductsModel> {
            override fun onResponse(call: Call<FeaturedProductsModel>, response: Response<FeaturedProductsModel>) {
                Log.d("FeaturedProductResponse", response.code().toString() + "")

                val resource = response?.body()

                val featuredProductsModel = FeaturedProductsModel()
                featuredProductsModel.products?.add(context.resources.getDrawable(R.drawable.featured_one))
                featuredProductsModel.products?.add(context.resources.getDrawable(R.drawable.featured_two))
                featuredProductsModel.products?.add(context.resources.getDrawable(R.drawable.featured_three))

                webServiceViewModel.sendFeaturedProductsResponse(featuredProductsModel)
            }

            override fun onFailure(call: Call<FeaturedProductsModel>, t: Throwable) {
                call.cancel()
            }
        })

    }


}