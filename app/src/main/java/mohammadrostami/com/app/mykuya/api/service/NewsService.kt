package mohammadrostami.com.app.mykuya.api.service

import android.content.Context
import android.util.Log
import mohammadrostami.com.app.mykuya.R
import mohammadrostami.com.app.mykuya.model.FeaturedProductsModel
import mohammadrostami.com.app.mykuya.model.NewsModel
import mohammadrostami.com.app.mykuya.model.ProductsModel
import mohammadrostami.com.app.mykuya.viewmodel.WebServiceViewModel
import mohammadrostami.com.app.welearnov2.apis.APIClient
import mohammadrostami.com.app.welearnov2.apis.APIInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewsService {
    private lateinit var webServiceViewModel: WebServiceViewModel
    private var apiInterface: APIInterface? = null

    fun main(context: Context, WebServiceViewModel: WebServiceViewModel) {
        this.webServiceViewModel = WebServiceViewModel


        apiInterface = APIClient.getClient()
            ?.create(APIInterface::class.java)


        val call = apiInterface?.getNews()
        call?.enqueue(object : Callback<NewsModel> {
            override fun onResponse(call: Call<NewsModel>, response: Response<NewsModel>) {
                Log.d("NewsResponse", response.code().toString() + "")

                val newsModel = NewsModel()
                newsModel.products?.add(context.resources.getDrawable(R.drawable.news_one))
                newsModel.products?.add(context.resources.getDrawable(R.drawable.news_two))

                webServiceViewModel.sendNewsResponse(newsModel)

            }

            override fun onFailure(call: Call<NewsModel>, t: Throwable) {
                call.cancel()
            }
        })

    }


}