package mohammadrostami.com.app.mykuya.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import mohammadrostami.com.app.mykuya.R
import mohammadrostami.com.app.mykuya.interfaces.OnItemListener

import kotlin.collections.ArrayList


//*************************************************************** THIS CLASS IS THE ADAPTER OF RECYCLERVIEWS
class RecyclerAdapter(
    private val context: Context,
    private val structs: ArrayList<Drawable>?,
    private val onItemListener: OnItemListener,
    private val Tab: Int
) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = parent.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        lateinit var view: View
        if (Tab == 1) {
            view = inflater.inflate(R.layout.row_product, parent, false)
        }
        if (Tab == 2) {
            view = inflater.inflate(R.layout.row_news, parent, false)
        }

        return ViewHolder(view)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (Tab == 1) {
            holder.imgLogo.setImageDrawable(structs?.get(position))
        }
        if (Tab == 2) {
            holder.imgLogo.setImageDrawable(structs?.get(position))

        }

    }


    override fun getItemCount(): Int {
        return structs!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val imgLogo: ImageView

        init {
            imgLogo = itemView.findViewById<View>(R.id.img_logo) as ImageView
        }

    }
}