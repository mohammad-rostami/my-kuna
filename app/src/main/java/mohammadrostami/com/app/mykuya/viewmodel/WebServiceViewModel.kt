package mohammadrostami.com.app.mykuya.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import mohammadrostami.com.app.mykuya.api.service.FeaturedProductsService
import mohammadrostami.com.app.mykuya.api.service.NewsService
import mohammadrostami.com.app.mykuya.api.service.ProductsService
import mohammadrostami.com.app.mykuya.model.FeaturedProductsModel
import mohammadrostami.com.app.mykuya.model.NewsModel
import mohammadrostami.com.app.mykuya.model.ProductsModel
import retrofit2.Response

class WebServiceViewModel : ViewModel() {

    private var featuredProducts: MutableLiveData<FeaturedProductsModel>? = MutableLiveData()
    private var products: MutableLiveData<ProductsModel>? = MutableLiveData()
    private var news: MutableLiveData<NewsModel>? = MutableLiveData()


    fun sendFeaturedProductsResponse(msg: FeaturedProductsModel) {
        featuredProducts?.setValue(msg)
    }

    fun getFeaturedProductsResponse(): LiveData<FeaturedProductsModel>? {
        return featuredProducts
    }

    fun callFeaturedProductsService(context: Context, webServiceViewModel: WebServiceViewModel) {

        val verify = FeaturedProductsService()
        verify.main(context,webServiceViewModel)
    }

    ////////////////////////////////////////////

    fun sendNewsResponse(msg: NewsModel) {
        news?.setValue(msg)
    }

    fun getNewsResponse(): LiveData<NewsModel>? {
        return news
    }

    fun callNewsService(context: Context, webServiceViewModel: WebServiceViewModel) {

        val verify = NewsService()
        verify.main(context,webServiceViewModel)
    }

    ///////////////////////////////////////////////


    fun sendProductsResponse(msg: ProductsModel) {
        products?.setValue(msg)
    }

    fun getProductsResponse(): LiveData<ProductsModel>? {
        return products
    }

    fun callProductsService(context: Context, webServiceViewModel: WebServiceViewModel) {

        val verify = ProductsService()
        verify.main(context,webServiceViewModel)
    }
}