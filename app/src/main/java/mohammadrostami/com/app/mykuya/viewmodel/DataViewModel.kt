package mohammadrostami.com.app.mykuya.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import mohammadrostami.com.app.mykuya.api.service.FeaturedProductsService
import mohammadrostami.com.app.mykuya.api.service.NewsService
import mohammadrostami.com.app.mykuya.api.service.ProductsService
import mohammadrostami.com.app.mykuya.model.FeaturedProductsModel
import mohammadrostami.com.app.mykuya.model.NewsModel
import mohammadrostami.com.app.mykuya.model.ProductsModel
import retrofit2.Response

class DataViewModel : ViewModel() {

    private var locationData: MutableLiveData<String>? = MutableLiveData()


    fun sendLocationResponse(msg: String) {
        locationData?.value = msg
    }

    fun getLocationResponse(): LiveData<String>? {
        return locationData
    }


}