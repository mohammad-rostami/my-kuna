package mohammadrostami.com.app.welearnov2.apis

import com.google.gson.JsonObject
import mohammadrostami.com.app.mykuya.model.FeaturedProductsModel
import mohammadrostami.com.app.mykuya.model.NewsModel
import mohammadrostami.com.app.mykuya.model.ProductsModel
import mohammadrostami.com.app.mykuya.util.Constant
import retrofit2.Call;
import retrofit2.http.*


interface APIInterface {


    @GET(Constant.GET_FEATURED_PRODUCTS)
    fun getFeaturedProducts(): Call<FeaturedProductsModel>

    @GET(Constant.GET_PRODUCTS)
    fun getProducts(): Call<ProductsModel>

    @GET(Constant.GET_NEWS)
    fun getNews(): Call<NewsModel>


}