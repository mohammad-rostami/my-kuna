package mohammadrostami.com.app.mykuya.util;

import mohammadrostami.com.app.mykuya.model.FeaturedProductsModel;

public class UrlManager {
    public static String getUrl(String url) {
        return Constant.BASE_URL + url;
    }

    public static String getApiUrl(String url) {
        return Constant.BASE_API_URL + url;
    }

    public static String getBaseUrl()
    {


        return Constant.BASE_API_URL;
    }
}
