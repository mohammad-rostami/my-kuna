package mohammadrostami.com.app.mykuya.model

import android.graphics.drawable.Drawable
import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose


open class NewsModel {

    @SerializedName("status")
    @Expose
    var status: Status? = null

    var products: ArrayList<Drawable>? = ArrayList()


    class Status {

        @SerializedName("Code")
        @Expose
        var code: Int? = null
        @SerializedName("Message")
        @Expose
        var message: String? = null

    }
}