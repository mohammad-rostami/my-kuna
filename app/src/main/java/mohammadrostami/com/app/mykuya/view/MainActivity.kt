package mohammadrostami.com.app.mykuya.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import mohammadrostami.com.app.mykuya.R
import mohammadrostami.com.app.mykuya.adapter.RecyclerAdapter
import mohammadrostami.com.app.mykuya.interfaces.OnItemListener
import mohammadrostami.com.app.mykuya.model.FeaturedProductsModel
import mohammadrostami.com.app.mykuya.model.NewsModel
import mohammadrostami.com.app.mykuya.model.ProductsModel
import mohammadrostami.com.app.mykuya.viewmodel.WebServiceViewModel
import android.net.Uri.fromParts
import android.content.Intent
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import mohammadrostami.com.app.mykuya.viewmodel.DataViewModel


class MainActivity : AppCompatActivity() {

    private lateinit var txtLocation: TextView
    private lateinit var flLocation: FrameLayout
    private lateinit var llLocation: LinearLayout
    private lateinit var rvNews: RecyclerView
    private lateinit var rvFeatured: RecyclerView
    private lateinit var rvProducts: RecyclerView
    private lateinit var FEATURED_PRODUTCS_ADAPTER: RecyclerAdapter
    private lateinit var PRODUTCS_ADAPTER: RecyclerAdapter
    private lateinit var NEWS_ADAPTER: RecyclerAdapter
    private lateinit var webServiceViewModel: WebServiceViewModel
    private lateinit var dataViewModel: DataViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        webServiceViewModel = ViewModelProviders.of(this).get(WebServiceViewModel::class.java)
        dataViewModel = ViewModelProviders.of(this).get(DataViewModel::class.java)
        rvFeatured = findViewById<View>(R.id.rv_featured) as RecyclerView
        rvProducts = findViewById<View>(R.id.rv_products) as RecyclerView
        rvNews = findViewById<View>(R.id.rv_news) as RecyclerView
        llLocation = findViewById<View>(R.id.ll_location) as LinearLayout
        flLocation = findViewById<View>(R.id.fl_location) as FrameLayout
        txtLocation = findViewById<View>(R.id.txt_location) as TextView

        getFeaturedProducts()
        getProducts()
        getNews()

        llLocation.setOnClickListener {
            val newFragment = FragmentLocation(dataViewModel)
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fl_location, newFragment)
            transaction.addToBackStack(null)
            transaction.commit()
            flLocation?.visibility = View.VISIBLE
        }

        dataViewModel.getLocationResponse()?.observe(this,
            Observer<String> {
                if (it != null) {
                    txtLocation.text = it
                    flLocation?.visibility = View.GONE

                }
            })


    }

    private fun getFeaturedProducts() {
        webServiceViewModel.callFeaturedProductsService(applicationContext, webServiceViewModel)
        webServiceViewModel.getFeaturedProductsResponse()?.observe(this,
            Observer<FeaturedProductsModel> {
                if (it != null) {
                    initFeaturedRecycler(it)
                }
            })
    }

    private fun getProducts() {
        webServiceViewModel.callProductsService(applicationContext, webServiceViewModel)
        webServiceViewModel.getProductsResponse()?.observe(this,
            Observer<ProductsModel> {
                if (it != null) {
                    initProductRecycler(it)
                }
            })
    }

    private fun getNews() {
        webServiceViewModel.callNewsService(applicationContext, webServiceViewModel)
        webServiceViewModel.getNewsResponse()?.observe(this,
            Observer<NewsModel> {
                if (it != null) {
                    initNewsRecycler(it)
                }
            })
    }

    private fun initFeaturedRecycler(it: FeaturedProductsModel) {
        FEATURED_PRODUTCS_ADAPTER = RecyclerAdapter(applicationContext, it.products, object : OnItemListener {
            override fun onItemSelect(position: Int) {
            }
        }, 1)
        val linearLayoutManagerOne = LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)
        rvFeatured.adapter = FEATURED_PRODUTCS_ADAPTER
        rvFeatured.layoutManager = linearLayoutManagerOne
    }

    private fun initProductRecycler(it: ProductsModel) {
        PRODUTCS_ADAPTER = RecyclerAdapter(applicationContext, it.products, object : OnItemListener {
            override fun onItemSelect(position: Int) {
            }
        }, 1)
        val gridLayoutManager = GridLayoutManager(applicationContext, 3)
        rvProducts.adapter = PRODUTCS_ADAPTER
        rvProducts.layoutManager = gridLayoutManager
    }

    private fun initNewsRecycler(it: NewsModel) {
        NEWS_ADAPTER = RecyclerAdapter(applicationContext, it.products, object : OnItemListener {
            override fun onItemSelect(position: Int) {
            }
        }, 2)
        val linearLayoutManagerOne = LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)
        rvNews.adapter = NEWS_ADAPTER
        rvNews.layoutManager = linearLayoutManagerOne
    }
}
