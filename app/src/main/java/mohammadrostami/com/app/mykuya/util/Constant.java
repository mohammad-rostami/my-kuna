package mohammadrostami.com.app.mykuya.util;

public class Constant {

    // Base Url
    public static final String BASE_URL = "https://9b4ec606-bd65-4f11-978e-7da29177faa2.mock.pstmn.io/";
    public static final String BASE_API_URL = "https://9b4ec606-bd65-4f11-978e-7da29177faa2.mock.pstmn.io/";


    public static final String GET_FEATURED_PRODUCTS = "getFeaturedProducts";
    public static final String GET_PRODUCTS = "getProducts";
    public static final String GET_NEWS = "getNews";





    // Strings
    public static final String registeredText = "Already a user. Login code has been sent.";
    public static final String verifyText = "Your number has been verified.";
    public static final String registerText = "Your user has been created";
}
