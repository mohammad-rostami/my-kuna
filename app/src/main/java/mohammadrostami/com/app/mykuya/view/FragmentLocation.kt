package mohammadrostami.com.app.mykuya.view


import android.Manifest
import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView

import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.Circle
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.GeoApiContext
import com.google.maps.GeocodingApi

import mohammadrostami.com.app.mykuya.R
import mohammadrostami.com.app.mykuya.viewmodel.DataViewModel
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.runOnUiThread
import java.text.DecimalFormat
import java.util.*


@SuppressLint("ValidFragment")
class FragmentLocation(val dataViewModel: DataViewModel) : Fragment(), View.OnClickListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, LocationListener, GoogleMap.OnMapLongClickListener {



    private lateinit var btnConfirm: Button
    private val defaultGlat = 35.6996863
    private val defaultGlng = 51.3374805

    private var mMap: GoogleMap? = null
    private val mGoogleApiClient: GoogleApiClient? = null
    private var LOCATION: LatLng? = null
    private val positionMarker: Marker? = null
    private val accuracyCircle: Circle? = null



    private val myRunnable: Runnable? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_location, container, false)

        val mFragmentMap = childFragmentManager.findFragmentById(R.id.fr_main) as SupportMapFragment?
        mFragmentMap!!.getMapAsync(this)


        var txtAddress = view.findViewById<View>(R.id.address) as TextView
        btnConfirm = view.findViewById<View>(R.id.btn_confirm) as Button
        btnConfirm.setOnClickListener {

            var centerLatLang = mMap?.projection!!.visibleRegion.latLngBounds.getCenter()

            var df: DecimalFormat = DecimalFormat()
            df.setMaximumFractionDigits(3)

            var lat = (df.format(centerLatLang.latitude)).toDouble()
            var lng = (df.format(centerLatLang.longitude)).toDouble()
            Log.d("location", lat.toString())
            Log.d("location", lng.toString())


            Thread {
                var geocoder = Geocoder(context, Locale.getDefault())
                var addresses: List<Address> = geocoder.getFromLocation(lat, lng, 1)
                var addressName = addresses[0].locality + ", " + addresses[0].featureName
//            var address = addresses.get(0).getAddressLine(0)
//            var city = addresses.get(0).getLocality()
//            var state = addresses.get(0).getAdminArea()
//            var country = addresses.get(0).getCountryName()
//            var postalCode = addresses.get(0).getPostalCode()
//            var knownName = addresses.get(0).getFeatureName()


                runOnUiThread {
                    //code that runs in main
                    txtAddress.text = addressName
                    dataViewModel.sendLocationResponse(addressName)
                }

            }.start()


        }


        return view
    }


    override fun onClick(view: View) {

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.e("Req Code", "" + requestCode)
        if (requestCode == REQUEST_CODE_PERMISSION) {
        }
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap!!.setOnMapLongClickListener(this)
        mMap!!.uiSettings.isMyLocationButtonEnabled = true


        if (ContextCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            mMap!!.isMyLocationEnabled = true
            mMap!!.uiSettings.isZoomControlsEnabled = true

        } else {
            // Show rationale and request permission.
        }
        mMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL

        LOCATION = LatLng(defaultGlat, defaultGlng)
        mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LOCATION, 14f))
//        mMap!!.addMarker(
//            MarkerOptions()
//                .position(LatLng(defaultGlat, defaultGlng))
//                .title("Hello world")
//        )
    }

    override fun onConnected(bundle: Bundle?) {
        val mLocationRequest = LocationRequest()
        mLocationRequest.interval = 1000
        mLocationRequest.fastestInterval = 1000
        mLocationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        if (ContextCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
        }
    }

    override fun onConnectionSuspended(i: Int) {

    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

    }

    /**
     * when location is changed this method moves camera to new location
     *
     * @param location
     */
    override fun onLocationChanged(location: Location) {

        val latitude = location.latitude
        val longitude = location.longitude
        val accuracy = location.accuracy

        positionMarker?.remove()

        accuracyCircle?.remove()

        LOCATION = LatLng(latitude, longitude)
        mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LOCATION, 14f))

    }

    override fun onMapLongClick(latLng: LatLng) {

    }

    override fun onDetach() {
        super.onDetach()

    }


    override fun onDestroyView() {
        super.onDestroyView()
    }

    companion object {
        private val REQUEST_CODE_PERMISSION = 2
    }


}// Required empty public constructor
