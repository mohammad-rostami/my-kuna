package mohammadrostami.com.app.welearnov2.apis

import mohammadrostami.com.app.mykuya.util.UrlManager
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.Response
import okio.IOException


class APIClient {

    companion object {

        private var retrofit: Retrofit? = null

        fun getClient(): Retrofit? {

            val httpClient = OkHttpClient.Builder().addInterceptor(object : Interceptor {
                @Throws(IOException::class)
                override fun intercept(chain: Interceptor.Chain): Response {
                    val request = chain.request().newBuilder().addHeader("hykey", "somesecretshitforthebetaapi").build()
                    return chain.proceed(request)
                }
            }).build()


            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client: OkHttpClient = OkHttpClient.Builder().addInterceptor(interceptor).build()


            retrofit = Retrofit.Builder()
                .baseUrl(UrlManager.getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()



            return retrofit
        }
    }
}