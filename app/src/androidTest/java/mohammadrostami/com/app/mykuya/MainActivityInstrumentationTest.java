package mohammadrostami.com.app.mykuya;

import android.os.Handler;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import mohammadrostami.com.app.mykuya.view.MainActivity;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.hamcrest.core.AllOf.allOf;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityInstrumentationTest {


    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void clickLocationButton_openLocationFragment() {
        //locate and click on the location button
        onView(withId(R.id.ll_location)).perform(click());

        //check that the location screen is displayed
        onView(withId(R.id.fl_location)).check(matches(isDisplayed()));
    }

}
